﻿using System;
using System.Net;

namespace Store.Common
{
    public class AjaxJsonData<T>
    {
        public HttpStatusCode StatusCode { get; set; }
        public T Data { get; set; }

        public static AjaxJsonData<T> Create()
        {
            return new AjaxJsonData<T>();
        }
    }
}