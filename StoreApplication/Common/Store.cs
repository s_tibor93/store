﻿using System;

namespace Store.Common
{
    public class Store
    {
        //***************************
        // Singleton
        //***************************

        private static Lazy<Store> m_instance = new Lazy<Store>(() => { return new Store(); }, true);
        public static Store Instance
        {
            get { return m_instance.Value; }
        }
        
        //***************************
        // Konstrukció
        //***************************

        private Store()
        { }

        //***************************
        // Property(k)
        //***************************

        public int NavigationSelectedId { get; set; }
    }
}