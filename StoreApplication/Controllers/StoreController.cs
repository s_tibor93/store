﻿using Store.Common;
using Store.Data;
using Store.Data.Repository;
using Store.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Store.Controllers
{
    public class StoreController : Controller
    {
        public ActionResult Index()
        {
            var model = new StoreViewModel();

            using (var work = new UnitOfWork(new StoreContext()))
            {
                var items = work.ProductRepository.GetAll();
                model.Products = items.Select((x) => (ItemViewModel)x);
                model.ProductGroups = work.ProductGroupRepository.GetAll();
            }

            return View("Index", model);
        }

        /// <summary>
        /// Visszaadja egy cikkcsoportba és az összes 
        /// alcsoportjaiba tartozó összes elemet.
        /// </summary>
        /// <param name="id">Cikkcsoport ID-ja</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetItemsById(int itemGroupId)
        {
            IEnumerable<Product> selected;

            using (var work = new UnitOfWork(new StoreContext()))
            {
                var ids = new HashSet<int>();
                GetAllChildren(work.ProductGroupRepository, itemGroupId, ids);

                var items = new List<Product>();
                foreach (var id in ids)
                {
                    var i = work.ProductRepository.Find(e => e.ProductGroupId == id);
                    items.AddRange(i);
                }

                selected = items;
            }

            var send = AjaxJsonData<IEnumerable<Product>>.Create();
            send.StatusCode = HttpStatusCode.OK;
            send.Data = selected;

            return Json(send);
        }

        private void GetAllChildren(Repository<ProductGroup> repository, int groupItemId, ICollection<int> ids)
        {
            try
            {
                var groupItem = repository.GetById(groupItemId);
                ids.Add(groupItem.Id);

                foreach (var item in repository.Find(e => e.ParentId == groupItem.Id))
                {
                    GetAllChildren(repository, item.Id, ids);
                }
            }
            catch (Exception exp)
            { }
        }

        [HttpPost]
        public JsonResult GetItemInfo(int id)
        {
            Product selected;

            using (var ctx = new StoreContext())
            {
                selected = ctx.Product.SingleOrDefault(item => item.Id == id);
            }

            var send = new AjaxJsonData<Product>
            {
                StatusCode = HttpStatusCode.OK,
                Data = selected,
            };

            return Json(send);
        }

        [HttpPost]
        public JsonResult Update(Product item, HttpPostedFileBase image)
        {
            if (ModelState.IsValid)
            {
                using (var work = new UnitOfWork(new StoreContext()))
                {
                    var old = work.ProductRepository.GetById(item.Id);

                    if (image != null)
                    {
                        var dir = Path.GetDirectoryName(old.ImagePath);

                        var fullPath = Server.MapPath("~" + dir + "\\" + image.FileName);

                        image.SaveAs(fullPath);

                        item.ImagePath = dir + "\\" + image.FileName;
                    }
                    
                    work.ProductRepository.Delete(old);
                    work.ProductRepository.Add(item);
                    work.Commit();
                }
            }

            return Json((int)HttpStatusCode.OK);
        }

        [HttpPost]
        public JsonResult Create(Product item, HttpPostedFileBase image)
        {
            if (ModelState.IsValid)
            {
                using (var work = new UnitOfWork(new StoreContext()))
                {
                    if (image != null)
                    {
                        var path = "~/Resources/Images/";
                        var fullPath = Path.Combine(Server.MapPath(path), image.FileName);

                        image.SaveAs(fullPath);

                        item.ImagePath = "/Resources/Images/" + image.FileName;
                    }

                    work.ProductRepository.Add(item);
                    work.Commit();
                }
            }

            return Json((int)HttpStatusCode.OK);
        }

        public JsonResult Delete(int id)
        {
            using (var work = new UnitOfWork(new StoreContext()))
            {
                work.ProductRepository.DeleteById(220);
                work.ProductRepository.DeleteById(id);
                work.Commit();
            }

            return Json((int)HttpStatusCode.OK);
        }
    }
}