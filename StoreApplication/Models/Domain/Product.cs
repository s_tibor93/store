﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using System.Web;

namespace Store.Models
{
    public class Product
    {
        [Key]
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Item group number")]
        [StringLength(50)]
        public string ProductNumber { get; set; }

        [Required]
        [Display(Name = "Name")]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Selling price")]
        [Range(0, 9999999.99)]
        public decimal SellingPrice { get; set; }

        [Display(Name = "Description")]
        [StringLength(200)]
        public string Description { get; set; }

        [Display(Name = "Change image")]
        [StringLength(150)]
        public string ImagePath { get; set; }
        
        //[ForeignKey("ItemGroup")]
        [Display(Name = "Product group")]
        public int ProductGroupId { get; set; }

        //[JsonIgnore]
        //public ItemGroup ItemGroup { get; set; }
    }
}