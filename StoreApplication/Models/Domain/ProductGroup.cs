﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Store.Models
{
    public class ProductGroup
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int? ParentId { get; set; }

        public virtual ICollection<Product> Products { get; set; }

        public override string ToString()
        {
            return string.Format("Id: {0}; Name: {1}; ParentId: {2}", Id, Name, ParentId);
        }

        //public override string ToString() =>
            //$"Id: { Id }; Name: { Name }; ParentId: { ParentId }";
    }
}
