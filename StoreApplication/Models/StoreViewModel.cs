﻿using System;
using System.Collections.Generic;

namespace Store.Models
{
    public class StoreViewModel
    {
        public IEnumerable<ProductGroup> ProductGroups { get; set; }
        public IEnumerable<ItemViewModel> Products { get; set; }
    }
}