﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Store.Models
{
    public class ItemViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public HttpPostedFileBase ImagePath { get; set; }

        private ItemViewModel()
        { }

        public static ItemViewModel Create(Product item)
        {
            return new ItemViewModel
            {
                Id = item.Id,
                Name = item.Name,
                //ImagePath = item.ImagePath,
            };
        }

        public static IEnumerable<ItemViewModel> Create(IEnumerable<Product> items)
        {
            foreach (var item in items)
            {
                yield return Create(item);
            }
        }

        public static explicit operator ItemViewModel(Product item)
        {
            return Create(item);
        }
    }
}