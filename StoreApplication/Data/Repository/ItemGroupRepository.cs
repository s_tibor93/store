﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Store.Models;
using Store.Data.Repository;
using System.Data.Entity;

namespace Store.Data
{
    public class ItemGroupRepository : Repository<ProductGroup>
    {
        public StoreContext StoreContext
        {
            get { return Context as StoreContext; }
        }

        public ItemGroupRepository(StoreContext context) : base(context)
        { }
    }
}