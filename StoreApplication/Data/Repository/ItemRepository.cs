﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Store.Models;
using Store.Data.Repository;

namespace Store.Data
{
    public class ItemRepository : Repository<Product>
    {
        public StoreContext StoreContext
        {
            get { return Context as StoreContext; }
        }

        public ItemRepository(StoreContext context) : base(context)
        { }
    }
}