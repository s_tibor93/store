﻿using System;
using System.Threading.Tasks;
using Store.Models;
using Store.Data.Repository;

namespace Store.Data
{
    public class UnitOfWork : IDisposable
    {
        private StoreContext m_context { get; }

        public Repository<Product> ProductRepository { get; }
        public Repository<ProductGroup> ProductGroupRepository { get; }
        
        public UnitOfWork(StoreContext context)
        {
            m_context = context;

            ProductRepository = new Repository<Product>(context);
            ProductGroupRepository = new Repository<ProductGroup>(context);
        }

        public int Commit()
        {
            return m_context.SaveChanges();
        }

        public async Task<int> CommitAsync()
        {
            return await m_context.SaveChangesAsync();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            m_context.Dispose();
        }
    }
}